const express = require('express');
const axios = require('axios');
const yaml = require('js-yaml');
const fs = require('fs');
const app = express();
const port = 3000;

// Load configuration from config.yml
let config;
try {
    config = yaml.load(fs.readFileSync('config.yml', 'utf8'));
} catch (e) {
    console.error(e);
    process.exit(1);
}

const e621Username = config.e621.username;
const e621ApiKey = config.e621.api_key;

// Middleware to serve static files
app.use(express.static('public'));

// Route to fetch recent posts
app.get('/api/posts', async (req, res) => {
    try {
        const response = await axios.get('https://e621.net/posts.json', {
            params: { limit: 10 },
            headers: {
                'Authorization': 'Basic ' + Buffer.from(`${e621Username}:${e621ApiKey}`).toString('base64'),
                'User-Agent': 'MyProject/1.0 (by your_username on e621)'
            }
        });
        res.json(response.data);
    } catch (error) {
        console.error(error);
        res.status(500).send('Error fetching posts');
    }
});

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});
